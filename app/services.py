"""
Functions that implment features
"""

from components import Instruction

def read_document(file_name, file_encoding):
    """Reads the content of the document from the file and parse as
    content of lineal instructions.
    """
    with open(file_name, 'r', file_encoding) as lines:
        for _l in lines:
            detect_instruction(_l)

def detect_instruction(line, comment_tokens, delimiter):
    tokens = line.strip(delimiter)
    if len(tokens) > 3:
        comment = tokens[0]
        domain = tokens[1]
        instruction = tokens[2]
        arguments = tokens[3,]
        if comment in comment_tokens:
            Instruction(detect_domain(domain),
                        detect_instruction(instruction),
                        arguments)
            if Instruction.domain is not AsUnknown.Domain and Instruction.name is not AsUnknown.InstructionName:
                return Instruction
            else:
                AsInstruction.NotValid
                
