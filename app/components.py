"""
Essential data structures
"""
from collections import namedtuple

class Library:
    """A collection of parameters common to all documents that belong together

    The initial settings are retrieved from the app configuration available from
    a persistent storage.

    """
    pass

class Scheme:
    """A unit of information with imbedded instructions

    A scheme may contain fragments that
    belong to different versions (aka. projects). A document belongs to a
    library (extended version of a directory) and combines all version specific
    information. A document cannot be

    """

    def __init__(self, library):
        self.library = Library(library)

class Document:
    """A subset of a scheme which contains only the information applicable to a
    single version
    """
    def __init__(self, version, scheme):
        pass

class Variant:
    """A fragment of a document which was retrieved according to a document
    instruction
    """
    def __init__(self):
        pass
