.. _lineal.overview:

================================================================================
About Lineal
================================================================================

To create different variants you insert special instructions to your base file
as comments. All instructions include the following components delimited by
whitespace:

1. Comment token
2. Domain prefix
3. Instruction name
4. Instruction parameters

Instructions
================================================================================

.. content::
   :local:

domain domain-name
--------------------------------------------------------------------------------

The `domain` is a keyword that appears in comments of your files and marks all
lines which should be treated as instructions. The domain is mandatory for all
instructions.

begin
--------------------------------------------------------------------------------

The `begin` instruction declares a variant specific to the active domain. The
declared variant remains active unless:

1. The ~end~ declaration is used.
2. The end of the file is reached.

end
--------------------------------------------------------------------------------

The `end` instruction ends one or more variants passed as arguments. The `end`
instruction only applies to the projects introduced in the current file.

include
--------------------------------------------------------------------------------

The ~include~ instruction lists files that contain special instructions. The
included files are run at this point in the order they are listed from left to
right. In other words, ~# domain-name include file-1 file-2 file-2~ is identical
to:

# domain-name include file-1
# domain-name include file-2
# domain-name include file-3

skip
--------------------------------------------------------------------------------

The `skip` instruction effectively removes the instruction content from the
active variants.


