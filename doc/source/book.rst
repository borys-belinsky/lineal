.. _lineal.chapters:

================================================================================
Lineal - Line-by-Line Preprocessor for Plain Files
================================================================================

Lineal is a preprocessor for text files that enables generating multiple
variants of the same file in different locations.

.. toctree::

   overview
